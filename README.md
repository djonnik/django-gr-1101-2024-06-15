# django_gr1101_2024_05_30

---

[video 1](https://youtu.be/58N1YS0-Nss) - intro, Django project, django app, structure, settings

[video 2](https://youtu.be/lBUDOGei7PA) - models, migrations, fields

[video 3](https://youtu.be/tQzQwnFHVuA) - models, backwards relationships, admin/superuser, .save(), .objects()

[video 4](https://youtu.be/Qqd4uAgRyV4) - services, lookups, .filter(), .get(), .all()
